import { ConsoleLog } from '../config/logger.config';
import { ValidAdress, ValidatedWallet } from '../models/modelWallet';
import { walletService } from '../services/walletService';
import { convertEthToUsd, getEthCurrentUSDValue, sortAddressesDESC } from '../utils/utilWallet';
import { APIErrorResponse } from '../utils/utilResponse';
import { CheckWalletValidator } from '../validators/wallet';

const controllerWallet = {
  /**
   * Controller for create purchase order
   * @param _req
   * @param _res
   * @param _next
   */
  checkWalletBalance: async (_req, _res) => {
    try {
      ConsoleLog.http(`Service called.`, {
        service: 'controller/controllerWallet/checkWalletBalance',
      });
      const validatedInput: ValidatedWallet = await CheckWalletValidator(_req.body);

      if (validatedInput.validAddresses?.length > 0) {
        const ethUSDPrice = await getEthCurrentUSDValue();
        const invalidAddresses: string[] = [];
        const validAddresses: ValidAdress[] = [];

        await Promise.all(
          validatedInput.validAddresses.map(async (address: string) => {
            const balanceResult = await walletService.getWalletBalance(address);
            if (balanceResult.isValid) {
              validAddresses.push({
                address,
                ...convertEthToUsd(ethUSDPrice, balanceResult.ethBalance),
              });
            } else {
              invalidAddresses.push(address);
            }
          }),
        );

        const result = {
          wrongAddresses: [...invalidAddresses, ...validatedInput.invalidAdresses],
          sortedAddresses: sortAddressesDESC(validAddresses),
        };

        return _res.status(200).json(result);
      }

      const result = {
        wrongAddresses: validatedInput.invalidAdresses,
        sortedAddresses: [],
      };

      return _res.status(200).json(result);
    } catch (error) {
      ConsoleLog.error(`Unexpected error occurred.`, {
        service: 'controller/controllerWallet/checkWalletBalance',
        error,
      });
      _res.status(400).json(APIErrorResponse(error));
    }
  },
};

export { controllerWallet };
