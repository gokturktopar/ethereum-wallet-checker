import axios from 'axios';
import { CONST } from '../config/constants.config';
import { ConsoleLog } from '../config/logger.config';
import { ConvertEthToUsd, ValidAdress } from '../models/modelWallet';
const getEthCurrentUSDValue = async (): Promise<number> => {
  try {
    const response = await axios.get(CONST.ETH_USD_CONVERT_URL);
    const result = response?.data?.ethereum?.usd;
    if (result) return result;
    throw new Error('Error retrieving price');
  } catch (error) {
    ConsoleLog.error(`Unexpected error occurred.`, {
      service: 'service/utilWallet/checkWalletBalance',
      error,
    });
    throw new Error(error);
  }
};
const convertEthToUsd = (exchangeRate: number, ethBalance: number): ConvertEthToUsd => {
  try {
    return {
      ethBalance: parseFloat(ethBalance.toFixed(2)),
      usdBalance: parseFloat((exchangeRate * ethBalance).toFixed(2)),
    };
  } catch (error) {
    ConsoleLog.error(`Unexpected error occurred.`, {
      service: 'service/utilWallet/convertEthToUsd',
      error,
    });
    throw new Error(error);
  }
};
const sortAddressesDESC = (addresses: ValidAdress[]) =>
  addresses.sort((a, b) =>
    a.usdBalance > b.usdBalance ? -1 : b.usdBalance > a.usdBalance ? 1 : 0,
  );

export { getEthCurrentUSDValue, convertEthToUsd, sortAddressesDESC };
