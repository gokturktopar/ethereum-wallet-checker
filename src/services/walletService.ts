import { CONST } from '../config/constants.config';
import { ConsoleLog } from '../config/logger.config';
import { ethers, formatEther } from 'ethers';
import { WalletBalance } from '../models/modelWallet';
const provider = new ethers.JsonRpcProvider(CONST.ETH_NODE_URL);

const walletService = {
  getWalletBalance: async (walletAddress: string): Promise<WalletBalance> => {
    try {
      ConsoleLog.http(`Service called.`, {
        service: 'service/walletService/checkWalletBalance',
        walletAddress,
      });

      return new Promise<WalletBalance>((resolve, reject) => {
        provider
          .getBalance(walletAddress)
          .then((balance) => {
            const balanceInEth = parseFloat(formatEther(balance));
            resolve({
              isValid: true,
              ethBalance: balanceInEth,
            });
          })
          .catch((err) => {
            if (err.code === 'INVALID_ARGUMENT') {
              return resolve({ isValid: false });
            }
            reject(err);
          });
      });
    } catch (error) {
      ConsoleLog.error(`Unexpected error occurred.`, {
        service: 'service/walletService/checkWalletBalance',
        error,
      });
      throw new Error(error);
    }
  },
};

export { walletService };
