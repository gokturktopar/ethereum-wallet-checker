import { assert } from 'chai';
import { MOCK_DATA } from '../../src/config/test.config';
import { walletService } from '../../src/services/walletService';
import { getEthCurrentUSDValue, sortAddressesDESC } from '../../src/utils/utilWallet';
import { CheckWalletValidator } from '../../src/validators/wallet';

describe('checkWalletBalance/CheckWalletValidator', () => {
  it('should validate input data with only valid addresses', async () => {
    const result = await CheckWalletValidator(
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_ONLY_VALID_ADDRESSES.INPUT,
    );

    assert.property(result, 'invalidAdresses');
    assert.property(result, 'validAddresses');
    assert.equal(result.invalidAdresses.length, 0);
    assert.equal(result.validAddresses.length, 2);
    assert.deepEqual(
      result.validAddresses,
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_ONLY_VALID_ADDRESSES.INPUT,
    );
  });
  it('should validate input data with only invalid addresses', async () => {
    const result = await CheckWalletValidator(
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_ONLY_INVALID_ADDRESSES.INPUT,
    );

    assert.property(result, 'invalidAdresses');
    assert.property(result, 'validAddresses');
    assert.equal(result.invalidAdresses.length, 2);
    assert.equal(result.validAddresses.length, 0);
    assert.deepEqual(
      result.invalidAdresses,
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_ONLY_INVALID_ADDRESSES.INPUT,
    );
  });
  it('should validate input data with both valid&invalid addresses', async () => {
    const result = await CheckWalletValidator(
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_BOTH_VALID_INVALID_ADDRESSES.INPUT,
    );

    assert.property(result, 'invalidAdresses');
    assert.property(result, 'validAddresses');
    assert.equal(result.invalidAdresses.length, 2);
    assert.equal(result.validAddresses.length, 2);
    assert.deepEqual(
      result.invalidAdresses,
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_BOTH_VALID_INVALID_ADDRESSES.INVALID_RESULT,
    );
    assert.deepEqual(
      result.validAddresses,
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_BOTH_VALID_INVALID_ADDRESSES.VALID_RESULT,
    );
  });
  it('should eliminate duplicated addresses', async () => {
    const result = await CheckWalletValidator(
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_DUPLICATED_ADDRESSES.INPUT,
    );

    assert.property(result, 'invalidAdresses');
    assert.property(result, 'validAddresses');
    assert.equal(result.invalidAdresses.length, 0);
    assert.equal(result.validAddresses.length, 2);
    assert.deepEqual(
      result.validAddresses,
      MOCK_DATA.UNIT_TEST.VALIDATION.CASE_DUPLICATED_ADDRESSES.RESULT,
    );
  });
});

describe('checkWalletBalance/getEthCurrentUSDValue', () => {
  it('should get current usd value as number', async () => {
    const result = await getEthCurrentUSDValue();

    assert.isNumber(result);
  });
});

describe('checkWalletBalance/getWalletBalance', () => {
  it('should get wallet balance for valid address', async () => {
    const result = await walletService.getWalletBalance(
      MOCK_DATA.UNIT_TEST.WALLET_BALANCE.CASE_VALID_ADDRESS.INPUT,
    );

    assert.property(result, 'isValid');
    assert.property(result, 'ethBalance');
    assert.isNumber(result.ethBalance);
    assert.equal(result.isValid, true);
  });
  it('should get wallet balance for invalid address', async () => {
    const result = await walletService.getWalletBalance(
      MOCK_DATA.UNIT_TEST.WALLET_BALANCE.CASE_INVALID_ADDRESS.INPUT,
    );

    assert.property(result, 'isValid');
    assert.notProperty(result, 'ethBalance');
    assert.equal(result.isValid, false);
  });
});

describe('checkWalletBalance/sortAddresses', () => {
  it('should sort wallet addresses correctly', async () => {
    const result = sortAddressesDESC(MOCK_DATA.UNIT_TEST.ADDRESS_SORT.CASE_CORRECTLY_SORTED.INPUT);
    assert.deepEqual(result, MOCK_DATA.UNIT_TEST.ADDRESS_SORT.CASE_CORRECTLY_SORTED.RESULT);
  });
});
