import { createLogger, format, transports } from 'winston';

const serviceName = 'http://wallet-service';

const ConsoleLog = createLogger({
  level: 'debug',
  format: format.combine(
    format.timestamp({
      format: 'DD-MM-YYYY HH:mm:ss',
    }),
    format.colorize(),
    format.printf((args) => {
      const { level, message } = args;
      const meta: { service? } = Object.keys(args)
        .filter(function (key) {
          return ['level', 'message', 'label', 'timestamp'].indexOf(key) === -1;
        })
        .reduce(function (obj2, key) {
          obj2[key] = args[key];

          return obj2;
        }, {});
      meta.service = `${serviceName}/${meta.service || ''}`;

      return `${new Date().toISOString()} ${level}: ${message} ${
        Object.keys(meta).length !== 0 ? `- ${JSON.stringify(meta)}` : ''
      }`;
    }),
    format.errors({ stack: true }),
    format.splat(),
  ),
  transports: [
    new transports.Stream({
      stream: process.stderr,
      level: 'debug',
    }),
  ],
});

export { ConsoleLog };
