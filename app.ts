/**
 * Main file that performs server operations
 */

// Node-modules
import express from 'express';
import * as bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './doc/doc.json';
import { CONST } from './src/config/constants.config';
import { ConsoleLog } from './src/config/logger.config';
import { routeWallet } from './src/routes/routeWallet';
const app = express();
// support parsing of application/json type post data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Connect routes
app.use('/wallet', routeWallet);

app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

(async () => {
  // Start server
  const port = CONST.PORT;
  app.listen(port, () => {
    ConsoleLog.info(`Server running at http://localhost:${port}/`);
  });
})();
export default app;
