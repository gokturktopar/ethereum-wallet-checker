type CheckWalletInput = string[];
type ValidatedWallet = {
  invalidAdresses: string[];
  validAddresses: string[];
};

type ValidAdress = {
  address: string;
  ethBalance: number;
  usdBalance: number;
};

type WalletBalance = {
  isValid: boolean;
  ethBalance?: number;
};
type ConvertEthToUsd = {
  ethBalance: number;
  usdBalance: number;
};

export { CheckWalletInput, ValidatedWallet, ValidAdress, WalletBalance, ConvertEthToUsd };
