import * as Joi from '@hapi/joi';
import { VALIDATION } from '../config/constants.config';
import { ConsoleLog } from '../config/logger.config';
import { CheckWalletInput, ValidatedWallet } from '../models/modelWallet';

const CheckWalletValidator = async (input: CheckWalletInput): Promise<ValidatedWallet> => {
  try {
    const schema = Joi.array().items(Joi.any());

    await schema.validateAsync(input, { abortEarly: false });

    const invalidAdresses = [];
    const validAddresses = [];
    input?.forEach((address: string) => {
      const valid = VALIDATION.WALLET_ADDRESS_REGEX_PATTERN.test(address);
      if (valid) {
        validAddresses.push(address);
      } else {
        invalidAdresses.push(address);
      }
    });

    //remove duplicates
    const uniqueValidAddresses = validAddresses.filter((element, index) => {
      return validAddresses.indexOf(element) === index;
    });

    return {
      invalidAdresses,
      validAddresses: uniqueValidAddresses,
    };
  } catch (error) {
    ConsoleLog.error(`Unexpected error occurred.`, {
      service: 'validator/wallet/CheckWalletValidator',
      error,
    });

    throw new Error(error);
  }
};

export { CheckWalletValidator };
