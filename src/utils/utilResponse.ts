const APIErrorResponse = (errorMessage: string) => ({
  status: 400,
  errorMessage: parseErrorMessage(errorMessage),
});

const parseErrorMessage = (err) => {
  try {
    const error = new Error(err);
    const message = error?.message?.split('Error:').join('').trim();

    return message;
  } catch (error) {
    return err;
  }
};

export { APIErrorResponse };
