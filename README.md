# ETH Wallet Balance Checker

This project requires Nodejs +V14.

# Getting started

1. Go to project folder and install dependencies:

```sh
npm install
```

2. Launch development server, and open `localhost:3000` in your browser:

```sh
npm start
```

3. Launch development server, and open `localhost:3000/doc` in your browser, hitting that path provide api documentation.

```sh
npm start
```

4.  Runs all tests

```sh
npm test
```

5.  Runs e2e tests

```sh
npm run test:e2e
```

6.  Runs unit tests

```sh
npm run test:unit
```
