import { assert } from 'chai';
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../app';
import { MOCK_DATA } from '../../src/config/test.config';
chai.use(chaiHttp);

describe('checkWalletBalance', () => {
  const PATH = '/wallet/balance';
  it('should check wallet balance with valid addresses', async () => {
    const { body, status } = await chai
      .request(app)
      .post(PATH)
      .send(MOCK_DATA.E2E_TEST.CASE_VALID_ADDRESSES.INPUT);
    assert.equal(status, 200);

    assert.property(body, 'wrongAddresses');
    assert.property(body, 'sortedAddresses');
    assert.equal(body.wrongAddresses.length, 0);
    assert.equal(body.sortedAddresses.length, 2);
    assert.property(body.sortedAddresses[0], 'address');
    assert.property(body.sortedAddresses[0], 'ethBalance');
    assert.property(body.sortedAddresses[0], 'usdBalance');

    assert.isNumber(body.sortedAddresses[0].ethBalance);
    assert.isNumber(body.sortedAddresses[0].usdBalance);
  });
  it('should check wallet balance with valid&invalid addresses', async () => {
    const { body, status } = await chai
      .request(app)
      .post(PATH)
      .send(MOCK_DATA.E2E_TEST.CASE_VALID_INVALID_ADDRESSES.INPUT);
    assert.equal(status, 200);

    assert.property(body, 'wrongAddresses');
    assert.property(body, 'sortedAddresses');
    assert.equal(body.wrongAddresses.length, 2);
    assert.equal(body.sortedAddresses.length, 2);
    assert.property(body.sortedAddresses[0], 'address');
    assert.property(body.sortedAddresses[0], 'ethBalance');
    assert.property(body.sortedAddresses[0], 'usdBalance');

    assert.isNumber(body.sortedAddresses[0].ethBalance);
    assert.isNumber(body.sortedAddresses[0].usdBalance);

    assert.deepEqual(
      body.wrongAddresses,
      MOCK_DATA.E2E_TEST.CASE_VALID_INVALID_ADDRESSES.RESULT.INVALID_ADDRESSES,
    );
  });
  it('should check wallet balance with invalid addresses', async () => {
    const { body, status } = await chai
      .request(app)
      .post(PATH)
      .send(MOCK_DATA.E2E_TEST.CASE_INVALID_ADDRESSES.INPUT);
    assert.equal(status, 200);

    assert.property(body, 'wrongAddresses');
    assert.property(body, 'sortedAddresses');
    assert.equal(body.wrongAddresses.length, 2);
    assert.equal(body.sortedAddresses.length, 0);
    assert.deepEqual(
      body.wrongAddresses,
      MOCK_DATA.E2E_TEST.CASE_INVALID_ADDRESSES.RESULT.ADDRESSES,
    );
  });
  it('should check wallet balance with duplicated addresses', async () => {
    const { body, status } = await chai
      .request(app)
      .post(PATH)
      .send(MOCK_DATA.E2E_TEST.CASE_DUPLICATED_ADDRESSES.INPUT);
    assert.equal(status, 200);
    assert.property(body, 'wrongAddresses');
    assert.property(body, 'sortedAddresses');
    assert.equal(body.wrongAddresses.length, 0);
    assert.equal(body.sortedAddresses.length, 2);
    assert.property(body.sortedAddresses[0], 'address');
    assert.property(body.sortedAddresses[0], 'ethBalance');
    assert.property(body.sortedAddresses[0], 'usdBalance');

    assert.isNumber(body.sortedAddresses[0].ethBalance);
    assert.isNumber(body.sortedAddresses[0].usdBalance);
  });
  it('should give array invalid input type [string]', async () => {
    const { body, status } = await chai
      .request(app)
      .post(PATH)
      .send(MOCK_DATA.E2E_TEST.CASE_INVALID_INPUT.INPUT_STRING);
    assert.equal(status, 400);
    assert.equal(body.status, 400);
    assert.equal(body.errorMessage, 'Validation "value" must be an array');
  });
  it('should give array invalid input type [object]', async () => {
    const { body, status } = await chai
      .request(app)
      .post(PATH)
      .send(MOCK_DATA.E2E_TEST.CASE_INVALID_INPUT.INPUT_OBJECT);
    assert.equal(status, 400);
    assert.equal(body.status, 400);
    assert.equal(body.errorMessage, 'Validation "value" must be an array');
  });

  /*   it('should not calculate-basket without item field', async () => {
    const data = {
      ...MOCK_DATA.CALCULATE_BASKET_REQUEST_BODY_WITH_VALID_PROMO_CODE,
    };
    delete data.items;
    const { body, status } = await chai
      .request(app)
      .post(`/checkout/calculate-basket`)
      .send(data)
      .set(MOCK_DATA.REQUEST_HEADER);
    assert.equal(status, 400);
    assert.equal(body.code, 3406);
    assert.equal(body.desc, `ValidationError: "items" is required`);
  });
  it('should not calculate-basket without items required length', async () => {
    const data = {
      ...MOCK_DATA.CALCULATE_BASKET_REQUEST_BODY_WITH_VALID_PROMO_CODE,
    };
    const items = [...data.items];
    items.pop();
    const lastData = {
      items,
      promotionCode: data.promotionCode,
    };
    const { body, status } = await chai
      .request(app)
      .post(`/checkout/calculate-basket`)
      .send(lastData)
      .set(MOCK_DATA.REQUEST_HEADER);
    assert.equal(status, 400);
    assert.equal(body.code, 3406);
    assert.equal(body.desc, `ValidationError: "items" must contain at least 1 items`);
  });
  it('should not calculate-basket with invalid package', async () => {
    const data = {
      ...MOCK_DATA.CALCULATE_BASKET_REQUEST_BODY_WITH_VALID_PROMO_CODE,
    };
    data.items[0].key = 'INVALID_PACKAGE_KEY';
    const { body, status } = await chai
      .request(app)
      .post(`/checkout/calculate-basket`)
      .send(data)
      .set(MOCK_DATA.REQUEST_HEADER);
    assert.equal(status, 400);
    assert.equal(body.code, 3406);
    const keys = SUBCRIPTIONS_PACKAGES.map(({ name }) => name);
    assert.equal(body.desc, `ValidationError: "items[0].key" must be one of [${keys.join(', ')}]`);
  }); */
});
