import * as dotenv from 'dotenv';

dotenv.config();

const CONST = {
  PORT: 3000,
  ETH_NODE_URL: 'https://rpc.ankr.com/eth',
  ETH_USD_CONVERT_URL:
    'https://api.coingecko.com/api/v3/simple/price?ids=ethereum%2C&vs_currencies=usd',
};

const WALLET_ADDRESS_REGEX_PATTERN = new RegExp(/^(0x){1}[0-9a-fA-F]{40}$/i);

const VALIDATION = {
  WALLET_ADDRESS_REGEX_PATTERN,
};
export { CONST, VALIDATION };
