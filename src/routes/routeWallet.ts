import * as express from 'express';
import { controllerWallet } from '../controllers/controllerWallet';
const routeWallet = (() => {
  const router = express.Router();
  router.post('/balance', controllerWallet.checkWalletBalance);

  return router;
})();

export { routeWallet };
